package com.draf.cockroachdb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CockroachdbApplication {

	public static void main(String[] args) {
		SpringApplication.run(CockroachdbApplication.class, args);
	}

}
