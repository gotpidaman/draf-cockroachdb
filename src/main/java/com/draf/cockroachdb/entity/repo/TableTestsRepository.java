package com.draf.cockroachdb.entity.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.draf.cockroachdb.entity.TableTests;

@Repository
public interface TableTestsRepository extends JpaRepository<TableTests, Integer> {

}
