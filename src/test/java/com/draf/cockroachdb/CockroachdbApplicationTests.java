package com.draf.cockroachdb;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.draf.cockroachdb.entity.TableTests;
import com.draf.cockroachdb.entity.repo.TableTestsRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
class CockroachdbApplicationTests {

	@Autowired
	private TableTestsRepository tbRepo;
	
	@Test
	public void contextLoads() {
		try {
			TableTests tb = null;
			Calendar c = Calendar.getInstance(Locale.US);

			for(int i=0;i < 10;i++) {
				c.add(Calendar.DAY_OF_MONTH, 1);
				tb = new TableTests();
				tb.setId(i);
				tb.setNumber(2.2 * i);
				tb.setValue("ABC" + i);
				if(i%3 != 0) {
					tb.setDtUpdate(c.getTime());
				}
				tbRepo.save(tb);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
